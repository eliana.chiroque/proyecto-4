let estadoSemaforo = prompt("Dame un color").toLowerCase();

if (estadoSemaforo == "verde") {
  console.log("Puedes avanzar");
} else if (estadoSemaforo == "amarillo") {
 console.log("Avanza con precaución");
} else if (estadoSemaforo == "rojo") {
  console.log("Detente");
} else {
  console.log("Dame un color que sea válido");
}
